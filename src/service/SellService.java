package service;

import exception.ProductException;

public class SellService {

     public void sell(int money, String product) throws ProductException {
        if (money >= 100) {
            System.out.println("here is your " + product);
        } else
            throw new ProductException("you don't have enough money, please find extra " + (100 - money) + " dollars");
    }

}


package exception;

public class VegetableException extends ProductException {
    public VegetableException(String message) {
        super(message);
    }
}

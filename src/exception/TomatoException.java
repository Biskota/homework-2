package exception;

public class TomatoException extends VegetableException {
    public TomatoException(String message) {
        super(message);
    }
}

package products;

import exception.ProductException;
import service.SellService;

public class Product {

    public void sell(int money, String product) {
        try {
            SellService sellService = new SellService();
            sellService.sell(money, product);
        } catch (ProductException e) {
            System.out.println(e);
        }
    }

    public void throwAway(boolean isBad) throws ProductException {
        if (!isBad) {
            throw new ProductException("This product is good, better sell it");
        } else
        System.out.println("throw it faster");

    }

}


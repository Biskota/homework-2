package products;

import exception.ProductException;
import exception.TomatoException;
import exception.VegetableException;
import service.SellService;

public class Tomato extends Vegetable {

    @Override
    public void throwAway(boolean isBad) throws TomatoException {
        if (isBad) {
            System.out.println("throw it faster");
        }else throw new TomatoException("This tomato is good, better sell it");
    }

    @Override
    public void sell(int money, String tomato) {
        try {
            SellService sellService = new SellService();
            sellService.sell(money, tomato);
        } catch (TomatoException e) {
            System.out.println(e);
        } catch (VegetableException e2) {
            System.out.println(e2);
        } catch (ProductException e3) {
            System.out.println(e3);
        }

    }
}

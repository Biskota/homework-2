package products;

import exception.ProductException;
import exception.VegetableException;
import service.SellService;

public class Vegetable extends Product {

    @Override
    public void throwAway(boolean isBad) throws VegetableException{
        if (isBad) {
            System.out.println("throw it faster");
        }else throw new VegetableException("This vegetable is good, better sell it");
    }

    @Override
    public void sell(int money, String vegetable) {
        try {
            SellService sellService = new SellService();
            sellService.sell(money, vegetable);
        } catch (VegetableException e) {
            System.out.println(e);
        } catch (ProductException e2) {
            System.out.println(e2);
        }
    }


}

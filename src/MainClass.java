import exception.ProductException;
import products.Product;
import products.Tomato;
import products.Vegetable;

public class MainClass {
    public static void main(String[] args) throws ProductException {
        Product product = new Product();
        product.sell(17, "toy");
        product.sell(100, "toy");
        product.throwAway(true);
        //product.throwAway(false);

        System.out.println("==============");

        Product veg = new Vegetable();
        veg.sell(200, "grass");
        veg.sell(97, "gray grass");
        veg.throwAway(true);
        //veg.throwAway(false);

        System.out.println("==============");

        Product tomato = new Tomato();
        tomato.sell(96, "cherry tomato");
        tomato.sell(200, "best tomato");
        tomato.throwAway(true);
        //tomato.throwAway(false);

    }
}
